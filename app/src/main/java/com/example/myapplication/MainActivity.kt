package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt_button.setOnClickListener {
            if(vinaoba.text.isNullOrBlank()){
                Toast.makeText(this,"სახელი და გვარი არ არის შეყვანილი",Toast.LENGTH_SHORT).show()
            }
            if(piradinom.text.isNullOrBlank()){
                Toast.makeText(this, "პირადი ნომერი არ არის შეყვანილი",Toast.LENGTH_SHORT).show()
            }

            if(mob.text.isNullOrBlank()){
                Toast.makeText(this, "ტელეფონის ნომერი არ არის შეყვანილი",Toast.LENGTH_SHORT).show()
            }

            if(tarigi.text.isNullOrBlank()){
                Toast.makeText(this, "თარიღი არ არის მითითებული",Toast.LENGTH_SHORT).show()
            }
            if(muxli.text.isNullOrBlank()){
                Toast.makeText(this, "საჯარიმო მუხლი არ არის მითითებული",Toast.LENGTH_SHORT).show()
            }

            if(nomrebi.text.isNullOrBlank()){
                Toast.makeText(this, "მანქანის სახელმწიფო ნომრები არ არის მითითებული",Toast.LENGTH_SHORT).show()
            }

            if(check.isChecked()){
                Toast.makeText(this, "მოქალაქე დაჯარიმებულია",Toast.LENGTH_SHORT).show()
        }
            else {
                Toast.makeText(this, "თქვენ არ ეთანხმებით მოცემულ ინფორმაციას",Toast.LENGTH_SHORT).show()
            }
}
    }
}